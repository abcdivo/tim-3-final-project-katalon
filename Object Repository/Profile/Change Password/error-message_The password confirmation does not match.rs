<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>error-message_The password confirmation does not match</name>
   <tag></tag>
   <elementGuidId>8e39c15b-ae3d-491d-8de7-9d76e82243e7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>small</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/form/div[2]/span/small</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>small</value>
      <webElementGuid>94967269-8b6e-4cbd-8d53-1da0af2754a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The password confirmation does not match.</value>
      <webElementGuid>7d85eea1-1d51-42d5-90da-da7dc44aee8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;form-group&quot;]/span[@class=&quot;invalid-feedback&quot;]/small[1]</value>
      <webElementGuid>0ffc0a6f-82ae-45c0-9957-dc17c78d4e33</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div[2]/form/div[2]/span/small</value>
      <webElementGuid>929b3988-75ea-48be-bb14-2476e5da37d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='New Password'])[1]/following::small[1]</value>
      <webElementGuid>86869ae5-9e49-4f18-84c5-ecffbb18e44d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Old Password'])[1]/following::small[1]</value>
      <webElementGuid>1aaf8dff-a5e1-448d-9a82-a9d2de454d89</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirmation Password'])[1]/preceding::small[1]</value>
      <webElementGuid>5bc1435e-9643-450e-b485-5e7d0cb86089</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='The password confirmation does not match.'])[2]/preceding::small[1]</value>
      <webElementGuid>63f7ee09-4e83-4214-a19a-8fe4394dca68</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The password confirmation does not match.']/parent::*</value>
      <webElementGuid>057effad-f50c-410a-b800-10d56050faea</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//small</value>
      <webElementGuid>69ac2587-b9f6-4b62-8d53-d6e3e893e219</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//small[(text() = 'The password confirmation does not match.' or . = 'The password confirmation does not match.')]</value>
      <webElementGuid>516f26b6-e8e5-4616-81ec-4d5f2bb48464</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
