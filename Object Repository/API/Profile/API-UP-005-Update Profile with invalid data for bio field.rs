<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>API-UP-005-Update Profile with invalid data for bio field</name>
   <tag></tag>
   <elementGuidId>29d32005-54f1-46f7-83cb-48eb474811f4</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <authorizationRequest>
      <authorizationInfo>
         <entry>
            <key>bearerToken</key>
            <value>eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZWMyYTEzZWM5MTkwOTg3NGUyMTc1ZjE2N2EzNGZkMjRmNWM2MDY2OTU3NmMxMWQwNjVhOGU2YWYyZTRmNWJlYmM4YjVmYjBjMTBiY2FjMWMiLCJpYXQiOjE2OTgyNzc1MzYuNjUyNDYyLCJuYmYiOjE2OTgyNzc1MzYuNjUyNDY3LCJleHAiOjE3Mjk4OTk5MzYuNjQ0MDYyLCJzdWIiOiIxMTI3Iiwic2NvcGVzIjpbXX0.bgc_qj1DrxsnLfnen1ImhQ3kM5p_Yds2eGoAonx7YgEOXyQVvGvArs7vAFTr1L0FiG-xPItxBInnc5I109-5bloOwjqnjrWvZRyT3-uhcSXRulLilzr7FReHVMjPzHKbRCSUBnYOvI5upGPl34VhzSaBM4EZ0CgtHCV0e5IHzDnGPK_VsPXXn_y6Nw4U4-J4re6BoOMZ4gwi5MEzvJ5hE-RvmGpsR4gLYxURD_K6Ur7vY6iIg3OKvoKQo10_xYOUDiVvAqfjPwWiP9iGoFu69SG3ANr5WHwJARJ58DOHJE7-RAXUxtzaQltGZ5Ec-J_kbxdRMiwFy9md_w__jzyAUgTuPuUkL3bMeijCqc96w4m3cs15VvRTeq5dQGZanpETmE7V6SbhzMGX7AOsLuZrFvuEUkPNh7PLd4vmMhIvTOjRC9ECgYeWmu52vAS2l9N3sEsfy71FEdjbMxNDID-3OYFozynI7WzLPQzOgZApxYuBAjIJOR_uwsCo01jMSeUqXFOo2zMG_0ooPPN5eMUdW9bSZ-aTGCJ2Z5nmUTDjLs6ydlhP8BwJPwdgREX4nusBJsfWzfBZJrrgpw8PF5x3QMtK0lVUPwKNNiOa_4X8JiTBTjMTQObXTmjowdbs_RgI8bQmfqd-emroZG_ofWajBjv1vRYD-jNQhW1QS1Joc3g</value>
         </entry>
      </authorizationInfo>
      <authorizationType>Bearer</authorizationType>
   </authorizationRequest>
   <autoUpdateContent>true</autoUpdateContent>
   <connectionTimeout>0</connectionTimeout>
   <followRedirects>false</followRedirects>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;multipart/form-data&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;name&quot;,
      &quot;value&quot;: &quot;Putra&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;whatsapp&quot;,
      &quot;value&quot;: &quot;0812345678&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;birth_date&quot;,
      &quot;value&quot;: &quot;2000-07-18&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;bio&quot;,
      &quot;value&quot;: &quot;123456&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;position&quot;,
      &quot;value&quot;: &quot;mobile dev&quot;,
      &quot;type&quot;: &quot;Text&quot;,
      &quot;contentType&quot;: &quot;&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>form-data</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>multipart/form-data</value>
      <webElementGuid>e5ad1416-d3a4-48ed-a20f-ddcca5fd0aa2</webElementGuid>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIzIiwianRpIjoiZWMyYTEzZWM5MTkwOTg3NGUyMTc1ZjE2N2EzNGZkMjRmNWM2MDY2OTU3NmMxMWQwNjVhOGU2YWYyZTRmNWJlYmM4YjVmYjBjMTBiY2FjMWMiLCJpYXQiOjE2OTgyNzc1MzYuNjUyNDYyLCJuYmYiOjE2OTgyNzc1MzYuNjUyNDY3LCJleHAiOjE3Mjk4OTk5MzYuNjQ0MDYyLCJzdWIiOiIxMTI3Iiwic2NvcGVzIjpbXX0.bgc_qj1DrxsnLfnen1ImhQ3kM5p_Yds2eGoAonx7YgEOXyQVvGvArs7vAFTr1L0FiG-xPItxBInnc5I109-5bloOwjqnjrWvZRyT3-uhcSXRulLilzr7FReHVMjPzHKbRCSUBnYOvI5upGPl34VhzSaBM4EZ0CgtHCV0e5IHzDnGPK_VsPXXn_y6Nw4U4-J4re6BoOMZ4gwi5MEzvJ5hE-RvmGpsR4gLYxURD_K6Ur7vY6iIg3OKvoKQo10_xYOUDiVvAqfjPwWiP9iGoFu69SG3ANr5WHwJARJ58DOHJE7-RAXUxtzaQltGZ5Ec-J_kbxdRMiwFy9md_w__jzyAUgTuPuUkL3bMeijCqc96w4m3cs15VvRTeq5dQGZanpETmE7V6SbhzMGX7AOsLuZrFvuEUkPNh7PLd4vmMhIvTOjRC9ECgYeWmu52vAS2l9N3sEsfy71FEdjbMxNDID-3OYFozynI7WzLPQzOgZApxYuBAjIJOR_uwsCo01jMSeUqXFOo2zMG_0ooPPN5eMUdW9bSZ-aTGCJ2Z5nmUTDjLs6ydlhP8BwJPwdgREX4nusBJsfWzfBZJrrgpw8PF5x3QMtK0lVUPwKNNiOa_4X8JiTBTjMTQObXTmjowdbs_RgI8bQmfqd-emroZG_ofWajBjv1vRYD-jNQhW1QS1Joc3g</value>
      <webElementGuid>37157cf8-5e77-4f28-9bc3-216f622eb9cc</webElementGuid>
   </httpHeaderProperties>
   <katalonVersion>8.6.8</katalonVersion>
   <maxResponseSize>0</maxResponseSize>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>https://demo-app.online/api/updateprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceEndpoint></soapServiceEndpoint>
   <soapServiceFunction></soapServiceFunction>
   <socketTimeout>0</socketTimeout>
   <useServiceInfoFromWsdl>true</useServiceInfoFromWsdl>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
