<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Test Suites Login with DDT</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>2b7d9d31-8a19-4e56-bb02-00a7067c4372</testSuiteGuid>
   <testCaseLink>
      <guid>a52ee12d-bd5a-4ee6-887c-043a99e95481</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/WEB-LOG001-Email and Password DDT</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9e2b2ecf-f66e-4846-a2aa-d32cb6a9ca2c</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/Login with DDT</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>9e2b2ecf-f66e-4846-a2aa-d32cb6a9ca2c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Email</value>
         <variableId>20a83eac-7540-4109-9e90-82cc44857eaf</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>9e2b2ecf-f66e-4846-a2aa-d32cb6a9ca2c</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>Password</value>
         <variableId>331cb9ea-34f0-48ae-9549-ddb85ce77f85</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
